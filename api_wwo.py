#! /usr/bin/env python

import urllib, urllib.error, urllib.request
import json
from urllib.request import urlretrieve

class WeatherApi:
    def __init__(self):
        try:
            with open('weapyrc.json', 'r') as data_file:
                data = json.load(data_file)

        except FileNotFoundError:
            data = {'key':'', 'location':'', 'day':''}
            data['key'] = str(input("Enter WorldWeatherOnline API key: "))
            data['location'] = str(input("Enter location: "))
            data['day'] = int(input("Enter number of day: "))

            with open('weapyrc.json', 'w') as data_file:
                json.dump(data, data_file)

        self.api_key = data['key']
        self.api_url = "https://api.worldweatheronline.com/free/v2/weather.ashx"

    def get_weather(self, location, num_of_days, **kwargs):
        kwargs.update({
            "q": location,
            "num_of_days": num_of_days,
            "key": self.api_key,
            "tp" : 24,
            "format" :"json"
            })

        try:
            url = self.api_url + "?" + urllib.parse.urlencode(kwargs)
            data = urllib.request.urlopen(url)
            return json.loads(data.read().decode('utf-8'))
            data.close()

        except IOError:
            print("Api connection error")
            os.sys.exit()
