#! /usr/bin/env python
from __future__ import print_function
import os
import json
import api_wwo

def print_weather(weather, nd):
    we = weather['data']['weather']
    print(weather['data']['request'][0]['query']+"\n")
    for i in range(nd):
        print('%s             %s\n******************************* \
              \n%s    ------>   %s \
              \nTm: %s °C - TM: %s °C - FL: %s °C \
              \nWsp: %s Kmph %s - P: %s hPa  \
              \nprecipMM: %s mm -   Hum: %s %%\
              \n*******************************\n' % \
              (we[i]['date'], \
              we[i]['hourly'][0]['weatherDesc'][0]['value'].upper(), \
              we[i]['astronomy'][0]['sunrise'], \
              we[i]['astronomy'][0]['sunset'], \
              we[i]['mintempC'], we[i]['hourly'][0]['FeelsLikeC'], \
              we[i]['maxtempC'], we[i]['hourly'][0]['windspeedKmph'], \
              we[i]['hourly'][0]['winddir16Point'], \
              we[i]['hourly'][0]['pressure'], \
              we[i]['hourly'][0]['precipMM'], \
              we[i]['hourly'][0]['humidity'] \
             ),)

if __name__ == '__main__':

    request = api_wwo.WeatherApi()

    if len(os.sys.argv) == 1:
        with open('weapyrc.json', 'r') as data_file:
            data = json.load(data_file)
            location = data['location'] 
            nd = data['day'] 
    elif len(os.sys.argv) == 3:
        location = os.sys.argv[1]
        nd = int(os.sys.argv[2])
    else:
        print("Usage : python wea.py [location] [number_of_days]")
        os.sys.exit()
    wea = request.get_weather(location, nd)
    print_weather(wea, nd)
